package pro.applihelp.kmo.volontairs.domain.network.models

data class LoginModel(val token: String, val user: UserModel, val notif: NotificationModel) {

    var events: EventsShortModel? = EventsShortModel()
        get() = field ?: EventsShortModel()

}