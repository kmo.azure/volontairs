package pro.applihelp.kmo.volontairs.domain.network.services

import io.reactivex.Observable
import pro.applihelp.kmo.volontairs.domain.network.models.EmptyModel
import pro.applihelp.kmo.volontairs.domain.network.models.EventsModel
import pro.applihelp.kmo.volontairs.domain.network.models.LoginModel
import pro.applihelp.kmo.volontairs.domain.network.APIs
import retrofit2.http.*

interface BaseAPIs {

    @POST(APIs.REGISTRATION_POST)
    fun registrationPost(
            @Body user: LoginModel
    ): Observable<LoginModel>

    @GET(APIs.LOGIN_POST)
    fun loginPost(
            @Query("page") page: String
    ): Observable<EventsModel>

    @POST(APIs.LOGOUT_POST)
    fun logoutPost(): Observable<EmptyModel>

    @POST(APIs.REMIND_PASSWORD_POST)
    fun remindPassword(
            @Field("email") email: String
    ): Observable<EmptyModel>

    @POST(APIs.DONATE)
    fun donatePost(
            @Field("money") money: String
    ): Observable<EmptyModel>

}