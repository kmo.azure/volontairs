package pro.applihelp.kmo.volontairs.mvp.activities.reset_password

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter

@InjectViewState
class ResetPasswordPresenter: MvpPresenter<ResetPasswordView>() {
}