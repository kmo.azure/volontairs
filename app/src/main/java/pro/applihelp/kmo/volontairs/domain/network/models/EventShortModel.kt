package pro.applihelp.kmo.volontairs.domain.network.models

open class EventShortModel(
        val id: Long?,
        val type: String,
        val description: String,
        val is_my: Boolean,
        val coordinates: CoordinateModel,
        val date_time: DateTimeModel
) {

    constructor(eventShortModel: EventShortModel): this(
            eventShortModel.id,
            eventShortModel.type,
            eventShortModel.description,
            eventShortModel.is_my,
            eventShortModel.coordinates,
            eventShortModel.date_time
    )

}