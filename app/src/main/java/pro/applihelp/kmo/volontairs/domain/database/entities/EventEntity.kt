package pro.applihelp.kmo.volontairs.domain.database.entities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "event_data")
data class EventEntity(
        @PrimaryKey(autoGenerate = true) val id: Long,
        @ColumnInfo(name = "is_my") val is_my: Boolean
)