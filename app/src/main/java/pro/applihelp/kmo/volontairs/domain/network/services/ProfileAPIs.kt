package pro.applihelp.kmo.volontairs.domain.network.services

import io.reactivex.Observable
import pro.applihelp.kmo.volontairs.domain.network.models.UserModel
import pro.applihelp.kmo.volontairs.domain.network.APIs
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.PUT

interface ProfileAPIs {

    @FormUrlEncoded
    @PUT(APIs.PROFILE)
    fun profilePhotoPut(
            @Field("profile_photo") profile_photo: String
    ): Observable<UserModel>

    @FormUrlEncoded
    @PUT(APIs.PROFILE)
    fun profileFirstNamePut(
            @Field("first_name") first_name: String
    ): Observable<UserModel>

    @FormUrlEncoded
    @PUT(APIs.PROFILE)
    fun profileLastNamePut(
            @Field("last_name") last_name: String
    ): Observable<UserModel>

    @FormUrlEncoded
    @PUT(APIs.PROFILE)
    fun profilePhonePut(
            @Field("phone") phone: String
    ): Observable<UserModel>

    @FormUrlEncoded
    @PUT(APIs.PROFILE)
    fun profileEmailPut(
            @Field("email") email: String
    ): Observable<UserModel>

    @FormUrlEncoded
    @PUT(APIs.PROFILE)
    fun profilePasswordPut(
            @Field("password") password: String
    ): Observable<UserModel>

}