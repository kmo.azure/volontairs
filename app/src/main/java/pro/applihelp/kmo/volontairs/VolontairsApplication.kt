package pro.applihelp.kmo.volontairs

import android.app.Application
import pro.applihelp.kmo.volontairs.di.AppComponent
import pro.applihelp.kmo.volontairs.di.DaggerAppComponent

class VolontairsApplication: Application() {

    companion object {
        lateinit var INSTANCE: VolontairsApplication
    }

    private var appComponent: AppComponent? = null

    override fun onCreate() {
        super.onCreate()
        INSTANCE = this
    }

    fun getAppComponent(): AppComponent {
        if (appComponent == null) {
            appComponent = DaggerAppComponent.builder().build()
        }
        return appComponent!!
    }
}