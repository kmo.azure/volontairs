package pro.applihelp.kmo.volontairs.services.geolocation

import android.location.Location
import android.location.LocationListener
import android.os.Bundle

class LocationHelper(provider: String) : LocationListener {

    private val lastLocation: Location = Location(provider)

    override fun onLocationChanged(location: Location?) {
        lastLocation.set(location)
    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {

    }

    override fun onProviderEnabled(provider: String?) {

    }

    override fun onProviderDisabled(provider: String?) {

    }

}