package pro.applihelp.kmo.volontairs.domain.network.models

class EventsModel(val next: String? = "", val previous: String? = "",
                  val count: Int, val events: List<EventModel>? = ArrayList())