package pro.applihelp.kmo.volontairs.mvp.fragments.profile.change_password

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import ru.terrakok.cicerone.Router

@InjectViewState
class ChangePasswordPresenter(private val router: Router): MvpPresenter<ChangePasswordView>() {

    fun onBackPressed() {
        router.exit()
    }

}