package pro.applihelp.kmo.volontairs.domain.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import pro.applihelp.kmo.volontairs.domain.database.dao.EventDao
import pro.applihelp.kmo.volontairs.domain.database.dao.LoginDao
import pro.applihelp.kmo.volontairs.domain.database.entities.EventEntity
import pro.applihelp.kmo.volontairs.domain.database.entities.LoginEntity

@Database(entities = arrayOf(EventEntity::class, LoginEntity::class), version = 1, exportSchema = false)
abstract class AppDatabase: RoomDatabase() {

    abstract fun eventsDao(): EventDao

    abstract fun loginDao(): LoginDao

}