package pro.applihelp.kmo.volontairs.mvp.activities.loggedin

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import pro.applihelp.kmo.volontairs.navigation.Screens
import ru.terrakok.cicerone.Router

@InjectViewState
class LoggedInPresenter(private val router: Router): MvpPresenter<LoggedInView>() {
    fun changeTab(position: Int) {
        when(position) {
            0 -> router.replaceScreen(Screens.TAB_MAP_SCREEN)
            1 -> router.replaceScreen(Screens.TAB_CAN_I_HELP_SCREEN)
            2 -> router.replaceScreen(Screens.TAB_NEW_EVENT_SCREEN)
            3 -> router.replaceScreen(Screens.TAB_PROFILE_SCREEN)
            4 -> router.replaceScreen(Screens.TAB_SETTINGS_SCREEN)
        }
    }

    fun openDrawer() {
        viewState.openDrawer()
    }

    fun closeDrawer() {
        viewState.closeDrawer()
    }
}