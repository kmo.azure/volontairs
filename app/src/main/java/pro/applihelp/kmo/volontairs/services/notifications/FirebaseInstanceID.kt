package pro.applihelp.kmo.volontairs.services.notifications

import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdService


class FirebaseInstanceID: FirebaseInstanceIdService() {

    override fun onTokenRefresh() {
        super.onTokenRefresh()
        sendTokenToServer(token = FirebaseInstanceId.getInstance().token)
    }

    private fun sendTokenToServer(token: String?) {

    }
}