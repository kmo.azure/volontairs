package pro.applihelp.kmo.volontairs.navigation

import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class LocalNavigationModule {
    @Provides
    @Singleton
    fun provideLocalNavigationHolder() = LocalCiceroneHolder()
}