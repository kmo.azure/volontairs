package pro.applihelp.kmo.volontairs.domain.database.dao

import android.arch.persistence.room.*
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import pro.applihelp.kmo.volontairs.domain.database.entities.LoginEntity

@Dao
interface LoginDao {

    @Insert(onConflict = REPLACE)
    fun insert(login: LoginEntity)

    @Query(value = "SELECT * from user_data")
    fun getAllUsers(): List<LoginEntity>

    @Query(value = "SELECT * from user_data WHERE id = :id")
    fun getUser(id: Long): LoginEntity

    @Update(onConflict = REPLACE)
    fun updateUser(login: LoginEntity)

    @Delete
    fun deleteUser(login: LoginEntity)

}