package pro.applihelp.kmo.volontairs.mvp.fragments.events.events

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import pro.applihelp.kmo.volontairs.R
import pro.applihelp.kmo.volontairs.navigation.BackButtonListener

class EventsFragment: MvpAppCompatFragment(), EventsView, BackButtonListener {

    companion object {
        private val TAG = EventsFragment::getResources.name

        fun getNewInstance(name: String): EventsFragment {
            val fragment = EventsFragment()

            val args = Bundle()
            args.putString(TAG, name)
            fragment.arguments = args

            return fragment
        }
    }

    @InjectPresenter
    lateinit var presenter: EventsPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) = inflater.inflate(R.layout.fragment_events, container, false)!!

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onBackPressed(): Boolean {
        presenter.onBackPressed()
        return true
    }

}