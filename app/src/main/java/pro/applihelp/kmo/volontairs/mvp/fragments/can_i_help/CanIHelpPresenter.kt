package pro.applihelp.kmo.volontairs.mvp.fragments.can_i_help

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import ru.terrakok.cicerone.Router

@InjectViewState
class CanIHelpPresenter(private val router: Router): MvpPresenter<CanIHelpView>() {

    fun onBackPressed() {
        router.exit()
    }

}