package pro.applihelp.kmo.volontairs.mvp.fragments.settings.donate

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import ru.terrakok.cicerone.Router

@InjectViewState
class DonatePresenter(val router: Router): MvpPresenter<DonateView>() {

    fun onBackPressed() {
        router.exit()
    }

}