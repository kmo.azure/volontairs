package pro.applihelp.kmo.volontairs.di

import dagger.Component
import dagger.Module
import pro.applihelp.kmo.volontairs.mvp.activities.loggedin.LoggedInActivity
import pro.applihelp.kmo.volontairs.mvp.activities.login.LoginActivity
import pro.applihelp.kmo.volontairs.mvp.fragments.drawer_tabs.can_i_help.TabCanIHelpFragment
import pro.applihelp.kmo.volontairs.mvp.fragments.drawer_tabs.map.TabMapFragment
import pro.applihelp.kmo.volontairs.mvp.fragments.drawer_tabs.new_event.TabNewEventFragment
import pro.applihelp.kmo.volontairs.mvp.fragments.drawer_tabs.profile.TabProfileFragment
import pro.applihelp.kmo.volontairs.mvp.fragments.drawer_tabs.settings.TabSettingsFragment
import pro.applihelp.kmo.volontairs.navigation.LocalNavigationModule
import pro.applihelp.kmo.volontairs.navigation.NavigationModule
import javax.inject.Singleton

@Singleton
@Component(modules = [(NavigationModule::class), (LocalNavigationModule::class)])
interface AppComponent {

    fun inject(activity: LoginActivity)

    fun inject(activity: LoggedInActivity)

    fun inject(fragment: TabMapFragment)

    fun inject(fragment: TabCanIHelpFragment)

    fun inject(fragment: TabNewEventFragment)

    fun inject(fragment: TabProfileFragment)

    fun inject(fragment: TabSettingsFragment)

}