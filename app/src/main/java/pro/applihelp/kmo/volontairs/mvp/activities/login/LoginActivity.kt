package pro.applihelp.kmo.volontairs.mvp.activities.login

import android.os.Bundle
import android.util.Log
import com.arellomobile.mvp.MvpAppCompatActivity

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

import pro.applihelp.kmo.volontairs.R
import pro.applihelp.kmo.volontairs.domain.network.models.EventsModel
import pro.applihelp.kmo.volontairs.domain.network.NetworkHelper
import pro.applihelp.kmo.volontairs.domain.network.services.BaseAPIs

class LoginActivity: MvpAppCompatActivity(), LoginView {

    companion object {
        private val TAG = LoginActivity::getResources.name
    }

    private val compositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activit_login)

        getData()
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    private fun showData(data: EventsModel) {
        Log.e(TAG, "YEAH!!!")
    }

    private fun showError(error: String) {
        Log.e(TAG, "BAD $error")
    }

    private fun getData() {
        compositeDisposable.add(
                NetworkHelper
                        .getInstance()
                        ?.create(BaseAPIs::class.java)
                        ?.loginPost("1")
                        ?.subscribeOn(Schedulers.io())
                        ?.observeOn(AndroidSchedulers.mainThread())
                        ?.subscribe(
                                { showData(it) },
                                { showError(it.toString()) }
                        )!!
        )
    }
}