package pro.applihelp.kmo.volontairs.mvp.fragments.events.events

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import ru.terrakok.cicerone.Router

@InjectViewState
class EventsPresenter(private val router: Router): MvpPresenter<EventsView>() {

    fun onBackPressed() {
        router.exit()
    }

}