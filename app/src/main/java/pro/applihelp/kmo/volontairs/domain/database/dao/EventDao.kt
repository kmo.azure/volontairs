package pro.applihelp.kmo.volontairs.domain.database.dao

import android.arch.persistence.room.*
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import pro.applihelp.kmo.volontairs.domain.database.entities.EventEntity

@Dao
interface EventDao {

    @Insert(onConflict = REPLACE)
    fun insert(event: EventEntity)

    @Query(value = "SELECT * from event_data WHERE is_my = :is_my")
    fun getAllMyEvents(is_my: Boolean = true): List<EventEntity>

    @Query(value = "SELECT * from event_data")
    fun getAllEvents(): List<EventEntity>

    @Query(value = "SELECT * from event_data WHERE id = :id")
    fun getEvent(id: Long): EventEntity

    @Update(onConflict = REPLACE)
    fun updateEvent(event: EventEntity)

    @Delete
    fun deleteEvent(event: EventEntity)

}