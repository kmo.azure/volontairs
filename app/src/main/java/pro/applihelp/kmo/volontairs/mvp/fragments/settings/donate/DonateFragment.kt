package pro.applihelp.kmo.volontairs.mvp.fragments.settings.donate

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.ProvidePresenter
import pro.applihelp.kmo.volontairs.R
import pro.applihelp.kmo.volontairs.navigation.BackButtonListener
import pro.applihelp.kmo.volontairs.navigation.RouterProvider
import javax.inject.Inject

class DonateFragment: MvpAppCompatFragment(), DonateView, BackButtonListener {

    companion object {
        private val TAG = DonateFragment::getResources.name

        fun getNewInstance(name: String): DonateFragment {
            val fragment = DonateFragment()

            val args = Bundle()
            args.putString(TAG, name)
            fragment.arguments = args

            return fragment
        }

    }

    @Inject
    lateinit var presenter: DonatePresenter

    @ProvidePresenter
    fun providePresenter() = DonatePresenter(
            (parentFragment as RouterProvider).getRouter()
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) = inflater.inflate(R.layout.fragment_donate_fragment, container, false)!!

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onBackPressed(): Boolean {
        presenter.onBackPressed()
        return true

    }
}