package pro.applihelp.kmo.volontairs.navigation
import ru.terrakok.cicerone.Router;

interface RouterProvider {
    fun getRouter(): Router
}