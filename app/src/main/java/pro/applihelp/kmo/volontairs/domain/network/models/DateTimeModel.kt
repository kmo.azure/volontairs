package pro.applihelp.kmo.volontairs.domain.network.models

data class DateTimeModel (
        val start_time: String,
        val end_time: String,
        val start_date: String,
        val end_date: String
)