package pro.applihelp.kmo.volontairs.mvp.adapters.drawer

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import pro.applihelp.kmo.volontairs.R

class DrawerAdapter(private val items: List<DrawerItemsModel>) : RecyclerView.Adapter<DrawerItemViewHolder>() {

    interface ChoiceCallback {
        fun getPosition(position: Int)
    }

    lateinit var choiceCallback: ChoiceCallback

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrawerItemViewHolder {
        return DrawerItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.listitem_drawer, parent, false))
    }

    override fun onBindViewHolder(holder: DrawerItemViewHolder, position: Int) {
        val item = items[position]

        holder.icon.setImageResource(item.icon)
        holder.title.text = item.title

        holder.icon.setOnClickListener { choiceCallback.getPosition(position) }
        holder.title.setOnClickListener { choiceCallback.getPosition(position) }
    }

    override fun getItemCount() = items.size


}