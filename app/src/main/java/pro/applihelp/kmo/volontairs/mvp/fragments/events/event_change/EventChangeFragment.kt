package pro.applihelp.kmo.volontairs.mvp.fragments.events.event_change

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.ProvidePresenter
import pro.applihelp.kmo.volontairs.R
import pro.applihelp.kmo.volontairs.navigation.BackButtonListener
import pro.applihelp.kmo.volontairs.navigation.RouterProvider
import javax.inject.Inject

class EventChangeFragment: MvpAppCompatFragment(), EventChangeView, BackButtonListener {

    companion object {

        private val TAG = EventChangeFragment::getResources.name

        fun getNewInstance(name: String): EventChangeFragment {
            val fragment = EventChangeFragment()

            val args = Bundle()
            args.putString(TAG, name)
            fragment.arguments = args

            return fragment
        }

    }

    @Inject
    lateinit var presenter: EventChangePresenter

    @ProvidePresenter
    fun providePresenter() = EventChangePresenter(
            (parentFragment as RouterProvider).getRouter()
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) = inflater.inflate(R.layout.fragment_event_change, container, false)!!

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onBackPressed(): Boolean {
        presenter.onBackPressed()
        return true
    }

}