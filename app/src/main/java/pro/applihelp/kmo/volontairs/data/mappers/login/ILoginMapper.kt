package pro.applihelp.kmo.volontairs.data.mappers.login

interface ILoginMapper<D, E> {

    fun mapToEntity(type: D, is_login: Boolean): E

    fun mapFromEntity(type: E): D

}