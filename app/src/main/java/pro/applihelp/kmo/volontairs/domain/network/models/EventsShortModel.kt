package pro.applihelp.kmo.volontairs.domain.network.models

class EventsShortModel(val next: String? = "", val previous: String? = "",
                       val count: Int? = 0, val events: List<EventShortModel>? = ArrayList())