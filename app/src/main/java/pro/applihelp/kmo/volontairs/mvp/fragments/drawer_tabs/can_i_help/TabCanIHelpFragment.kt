package pro.applihelp.kmo.volontairs.mvp.fragments.drawer_tabs.can_i_help

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.ViewGroup
import pro.applihelp.kmo.volontairs.R
import pro.applihelp.kmo.volontairs.VolontairsApplication
import pro.applihelp.kmo.volontairs.mvp.fragments.can_i_help.CanIHelpFragment
import pro.applihelp.kmo.volontairs.navigation.BackButtonListener
import pro.applihelp.kmo.volontairs.navigation.LocalCiceroneHolder
import pro.applihelp.kmo.volontairs.navigation.RouterProvider
import pro.applihelp.kmo.volontairs.navigation.Screens
import ru.terrakok.cicerone.android.SupportAppNavigator
import javax.inject.Inject

class TabCanIHelpFragment: Fragment(), RouterProvider, BackButtonListener {

    companion object {
        val TAG = TabCanIHelpFragment::getResources.name

        fun getNewInstance(name: String): TabCanIHelpFragment {
            val fragment = TabCanIHelpFragment()

            val args = Bundle()
            args.putString(TAG, name)
            fragment.arguments = args

            return fragment
        }
    }

    @Inject
    lateinit var ciceroneHolder: LocalCiceroneHolder

    private var navigator: SupportAppNavigator? = null
    get() {
        if (field == null) {
            field = object : SupportAppNavigator(activity, childFragmentManager, R.id.container_can_i_help) {
                override fun createActivityIntent(context: Context?, screenKey: String?, data: Any?) = null

                override fun createFragment(screenKey: String?, data: Any?): Fragment? = when(screenKey) {
                    Screens.CAN_I_HELP_CATEGORY_SCREEN -> CanIHelpFragment.getNewInstance(getContainerName())
                    else -> null
                }

                override fun exit() = (activity as RouterProvider).getRouter().exit()
            }
        }

        return field
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        VolontairsApplication.INSTANCE.getAppComponent().inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) = inflater.inflate(R.layout.fragment_tab_can_i_help, container, false)!!

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (childFragmentManager.findFragmentById(R.id.container_can_i_help) == null) {
            getCicerone()!!.router.replaceScreen(Screens.CAN_I_HELP_CATEGORY_SCREEN)
        }
    }

    override fun onResume() {
        super.onResume()
        getCicerone()!!.navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        getCicerone()!!.navigatorHolder.removeNavigator()
        super.onPause()
    }

    override fun getRouter() = getCicerone()!!.router!!

    override fun onBackPressed(): Boolean {
        val fragment = childFragmentManager.findFragmentById(R.id.container_can_i_help)
        return if (fragment != null && fragment is BackButtonListener && (fragment as BackButtonListener).onBackPressed()) {
            true
        } else{
            (activity as RouterProvider).getRouter().exit()
            true
        }
    }

    private fun getContainerName() = arguments!!.getString(TAG)

    private fun getCicerone() = ciceroneHolder.getCicerone(getContainerName())

}