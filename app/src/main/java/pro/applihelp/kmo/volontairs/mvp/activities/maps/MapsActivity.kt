package pro.applihelp.kmo.volontairs.mvp.activities.maps

import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.NavigationView
import android.support.v4.widget.DrawerLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.Gravity
import com.arellomobile.mvp.MvpAppCompatActivity

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import pro.applihelp.kmo.volontairs.R
import pro.applihelp.kmo.volontairs.mvp.adapters.drawer.DrawerAdapter
import pro.applihelp.kmo.volontairs.mvp.adapters.drawer.DrawerItemsModel

class MapsActivity : MvpAppCompatActivity(), OnMapReadyCallback {

    private val TAG = "MAP ACTIVITY"
    private lateinit var mMap: GoogleMap

    //private lateinit var fab: FloatingActionButton
    //private lateinit var nav_view: NavigationView
    private lateinit var drawer: DrawerLayout
    private lateinit var list: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        //fab = findViewById(R.id.fab)
        drawer = findViewById(R.id.drawer_layout)
        list = findViewById(R.id.list)

        list.layoutManager = LinearLayoutManager(this)
        list.adapter = DrawerAdapter(arrayListOf(
                DrawerItemsModel(R.drawable.ic_launcher_background, "one"),
                DrawerItemsModel(R.drawable.ic_launcher_background, "two"),
                DrawerItemsModel(R.drawable.ic_launcher_background, "three")
        ))

        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        /*fab.setOnClickListener {
            if (drawer.isDrawerOpen(Gravity.END)) {
                drawer.closeDrawer(Gravity.END)
            } else{
                drawer.openDrawer(Gravity.END)
            }
        }*/
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        val mLocationManager = getSystemService(LOCATION_SERVICE) as LocationManager?

        try {
            mLocationManager?.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0L, 0f,
                    object : LocationListener {
                        override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
                            //To change body of created functions use File | Settings | File Templates.
                            Log.d(TAG, "onProviderEnabled $provider")
                        }

                        override fun onProviderEnabled(provider: String?) {
                            //To change body of created functions use File | Settings | File Templates.
                            Log.d(TAG, "onProviderEnabled $provider")
                        }

                        override fun onProviderDisabled(provider: String?) {
                            //To change body of created functions use File | Settings | File Templates.
                            Log.d(TAG, "onProviderDisabled $provider")
                        }

                        override fun onLocationChanged(location: Location) {
                            val lat = location.latitude
                            val lng = location.longitude

                            val tomsk = LatLng(lat, lng)

                            mMap.addMarker(
                                    MarkerOptions()
                                            .position(tomsk)
                                            .title("Marker in Sydney")
                            )

                            mMap.moveCamera(CameraUpdateFactory.newLatLng(tomsk))

                            Log.d(TAG, "Security Exception, no location available")
                        }
                    })
        } catch (ex: SecurityException) {
            Log.d(TAG, "Security Exception, no location available")
        }
    }
}