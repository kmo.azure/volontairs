package pro.applihelp.kmo.volontairs.mvp.adapters.drawer

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import pro.applihelp.kmo.volontairs.R

class DrawerItemViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    val icon: ImageView = itemView.findViewById(R.id.icon)
    val title: TextView = itemView.findViewById(R.id.title)
}