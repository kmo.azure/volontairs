package pro.applihelp.kmo.volontairs.mvp.fragments.events.event_map

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import ru.terrakok.cicerone.Router

@InjectViewState
class EventMapPresenter(private val router: Router): MvpPresenter<EventMapView>() {

    fun onBackPressed() {
        router.exit()
    }

}