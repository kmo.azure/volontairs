package pro.applihelp.kmo.volontairs.di

import android.arch.persistence.room.Room
import android.content.Context
import dagger.Module
import dagger.Provides
import pro.applihelp.kmo.volontairs.R
import pro.applihelp.kmo.volontairs.domain.database.AppDatabase

@Module
class DaoModule(private val context: Context) {

    @Provides
    fun provideAppContext() = context

    @Provides
    fun provideDataBase(context: Context) = Room.databaseBuilder(context, AppDatabase::class.java, context.resources.getString(R.string.volontairs_database)).allowMainThreadQueries().build()

    @Provides
    fun provideLoginDao(database: AppDatabase) = database.loginDao()

    @Provides
    fun provideEventDao(database: AppDatabase) = database.eventsDao()

}