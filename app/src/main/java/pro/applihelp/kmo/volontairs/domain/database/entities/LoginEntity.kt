package pro.applihelp.kmo.volontairs.domain.database.entities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "user_data")
data class LoginEntity(
        @PrimaryKey var id: Long,
        @ColumnInfo(name = "token") val token: String,
        @ColumnInfo(name = "email") val email: String,
        @ColumnInfo(name = "phone") val phone: String,
        @ColumnInfo(name = "profile_photo") val profile_photo: String,
        @ColumnInfo(name = "first_name") val first_name: String,
        @ColumnInfo(name = "last_name") val last_name: String,
        @ColumnInfo(name = "organization") val organization: String,
        @ColumnInfo(name = "notif_enable") val notif_enable: Boolean,
        @ColumnInfo(name = "notif_start") val notif_start: String,
        @ColumnInfo(name = "notif_end") val notif_end: String,
        @ColumnInfo(name = "is_login") val is_login: Boolean
)