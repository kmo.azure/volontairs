package pro.applihelp.kmo.volontairs.domain.network.models

class EventExtendedModel(
        event: EventModel,
        var contacts: List<ContactModel>
): EventModel(event = event)