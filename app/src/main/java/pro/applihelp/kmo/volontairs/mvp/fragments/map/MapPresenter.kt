package pro.applihelp.kmo.volontairs.mvp.fragments.map

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import ru.terrakok.cicerone.Router

@InjectViewState
class MapPresenter(val router: Router): MvpPresenter<MapView>() {

    fun onBackPressed() = router.exit()

}