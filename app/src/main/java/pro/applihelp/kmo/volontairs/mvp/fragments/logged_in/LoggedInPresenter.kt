package pro.applihelp.kmo.volontairs.mvp.fragments.logged_in

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter

@InjectViewState
class LoggedInPresenter: MvpPresenter<LoggedInView>() {
}