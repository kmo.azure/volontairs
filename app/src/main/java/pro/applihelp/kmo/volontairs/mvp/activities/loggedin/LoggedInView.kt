package pro.applihelp.kmo.volontairs.mvp.activities.loggedin

import com.arellomobile.mvp.MvpView

interface LoggedInView: MvpView {
    fun openDrawer()
    fun closeDrawer()
}