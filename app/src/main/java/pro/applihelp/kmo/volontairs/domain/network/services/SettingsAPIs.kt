package pro.applihelp.kmo.volontairs.domain.network.services

import io.reactivex.Observable
import pro.applihelp.kmo.volontairs.domain.network.models.LoginModel
import pro.applihelp.kmo.volontairs.domain.network.APIs
import retrofit2.http.Body
import retrofit2.http.FormUrlEncoded
import retrofit2.http.PUT

interface SettingsAPIs {

    @FormUrlEncoded
    @PUT(APIs.SETTINGS)
    fun settingsPut(
            @Body login: LoginModel
    ): Observable<LoginModel>

}