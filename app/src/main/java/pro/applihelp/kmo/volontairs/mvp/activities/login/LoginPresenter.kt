package pro.applihelp.kmo.volontairs.mvp.activities.login

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter

@InjectViewState
class LoginPresenter: MvpPresenter<LoginView>() {
}