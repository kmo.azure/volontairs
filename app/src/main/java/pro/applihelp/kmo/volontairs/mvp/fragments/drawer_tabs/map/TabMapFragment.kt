package pro.applihelp.kmo.volontairs.mvp.fragments.drawer_tabs.map

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.ViewGroup
import pro.applihelp.kmo.volontairs.R
import pro.applihelp.kmo.volontairs.VolontairsApplication
import pro.applihelp.kmo.volontairs.mvp.fragments.events.event_extended.EventExtendedFragment
import pro.applihelp.kmo.volontairs.mvp.fragments.events.events.EventsFragment
import pro.applihelp.kmo.volontairs.mvp.fragments.map.MapFragment
import pro.applihelp.kmo.volontairs.navigation.BackButtonListener
import pro.applihelp.kmo.volontairs.navigation.LocalCiceroneHolder
import pro.applihelp.kmo.volontairs.navigation.RouterProvider
import pro.applihelp.kmo.volontairs.navigation.Screens
import ru.terrakok.cicerone.android.SupportAppNavigator
import javax.inject.Inject

class TabMapFragment: Fragment(), RouterProvider, BackButtonListener {

    companion object {
        private val TAG = TabMapFragment::getResources.name

        fun getNewInstance(name: String): TabMapFragment {
            val fragment = TabMapFragment()

            val args = Bundle()
            args.putString(TAG, name)
            fragment.arguments = args

            return fragment
        }
    }

    @Inject
    lateinit var ciceroneHolder: LocalCiceroneHolder

    private var navigator: SupportAppNavigator? = null
    get() {
        if (field == null) {
            field = object : SupportAppNavigator(activity, childFragmentManager, R.id.container_map) {
                override fun createActivityIntent(context: Context, screenKey: String, data: Any?) = null

                override fun createFragment(screenKey: String?, data: Any?): Fragment? = when(screenKey) {
                    Screens.MAP_SCREEN -> MapFragment.getNewInstance(getContainerName())
                    Screens.EVENTS_SCREEN -> EventsFragment.getNewInstance(getContainerName())
                    Screens.EVENT_EXTENDED_SCREEN -> EventExtendedFragment.getNewInstance(getContainerName())
                    else -> null
                }

                override fun exit() = (activity as RouterProvider).getRouter().exit()
            }
        }

        return field
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        VolontairsApplication.INSTANCE.getAppComponent().inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) = inflater.inflate(R.layout.fragment_tab_map, container, false)!!

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (childFragmentManager.findFragmentById(R.id.container_map) == null) {
            getCicerone()!!.router.replaceScreen(Screens.MAP_SCREEN)
        }
    }

    override fun onResume() {
        super.onResume()
        getCicerone()!!.navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        getCicerone()!!.navigatorHolder.removeNavigator()
        super.onPause()
    }

    override fun getRouter() = getCicerone()!!.router!!

    override fun onBackPressed(): Boolean {
        val fragment = childFragmentManager.findFragmentById(R.id.container_map)
        return if (fragment != null && fragment is BackButtonListener && (fragment as BackButtonListener).onBackPressed()) {
            true
        } else{
            (activity as RouterProvider).getRouter().exit()
            true
        }
    }

    private fun getContainerName() = arguments!!.getString(TAG)

    private fun getCicerone() = ciceroneHolder.getCicerone(getContainerName())

}