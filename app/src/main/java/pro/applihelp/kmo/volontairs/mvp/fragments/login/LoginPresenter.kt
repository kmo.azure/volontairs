package pro.applihelp.kmo.volontairs.mvp.fragments.login

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter

@InjectViewState
class LoginPresenter: MvpPresenter<LoginView>() {
}