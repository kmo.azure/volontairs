package pro.applihelp.kmo.volontairs.mvp.fragments.events.event_extended

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import ru.terrakok.cicerone.Router

@InjectViewState
class EventExtendedPresenter(private val router: Router): MvpPresenter<EventExtendedView>() {

    fun onBackPressed() {
        router.exit()
    }

}