package pro.applihelp.kmo.volontairs.mvp.fragments.events.event_category

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import ru.terrakok.cicerone.Router

@InjectViewState
class EventCategoryPresenter(private val router: Router): MvpPresenter<EventCategoryView>() {

    fun onBackPressed() {
        router.exit()
    }

}