package pro.applihelp.kmo.volontairs.mvp.fragments.events.event_description

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import ru.terrakok.cicerone.Router

@InjectViewState
class EventDescriptionPresenter(private val router: Router): MvpPresenter<EventDescriptionView>() {

    fun onBackPressed() {
        router.exit()
    }

}