package pro.applihelp.kmo.volontairs.mvp.fragments.events.event_extended

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import pro.applihelp.kmo.volontairs.R
import pro.applihelp.kmo.volontairs.navigation.BackButtonListener
import pro.applihelp.kmo.volontairs.navigation.RouterProvider

class EventExtendedFragment: MvpAppCompatFragment(), EventExtendedView, BackButtonListener {

    companion object {
        private val TAG = EventExtendedFragment::getResources.name

        fun getNewInstance(name: String): EventExtendedFragment {
            val fragment = EventExtendedFragment()

            val args = Bundle()
            args.putString(TAG, name)
            fragment.arguments = args

            return fragment
        }
    }

    @InjectPresenter
    lateinit var presenter: EventExtendedPresenter

    @ProvidePresenter
    fun providePresenter() = EventExtendedPresenter(
            (parentFragment as RouterProvider).getRouter()
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) = inflater.inflate(R.layout.fragment_event_extended, container, false)!!

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onBackPressed(): Boolean {
        presenter.onBackPressed()
        return true
    }

}