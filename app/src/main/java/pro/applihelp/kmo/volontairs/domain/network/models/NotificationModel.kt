package pro.applihelp.kmo.volontairs.domain.network.models

data class NotificationModel(val notif_enable: Boolean, val notif_start: String, val notif_end: String)