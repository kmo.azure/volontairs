package pro.applihelp.kmo.volontairs.mvp.fragments.events.event_change

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import ru.terrakok.cicerone.Router

@InjectViewState
class EventChangePresenter(val router: Router): MvpPresenter<EventChangeView>() {

    fun onBackPressed() {
        router.exit()
    }

}