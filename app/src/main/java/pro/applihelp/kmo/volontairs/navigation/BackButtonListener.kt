package pro.applihelp.kmo.volontairs.navigation

interface BackButtonListener {
    fun onBackPressed(): Boolean
}