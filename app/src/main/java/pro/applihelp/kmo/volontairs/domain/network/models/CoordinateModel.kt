package pro.applihelp.kmo.volontairs.domain.network.models

data class CoordinateModel(
        val lng: String,
        val lat: String
)