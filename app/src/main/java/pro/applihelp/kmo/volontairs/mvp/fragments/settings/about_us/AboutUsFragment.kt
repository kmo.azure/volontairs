package pro.applihelp.kmo.volontairs.mvp.fragments.settings.about_us

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import pro.applihelp.kmo.volontairs.R
import pro.applihelp.kmo.volontairs.navigation.BackButtonListener
import pro.applihelp.kmo.volontairs.navigation.RouterProvider

class AboutUsFragment: MvpAppCompatFragment(), AboutUsView, BackButtonListener {

    companion object {
        private val TAG = AboutUsFragment::getResources.name

        fun getNewInstance(name: String): AboutUsFragment {
            val fragment = AboutUsFragment()

            val args = Bundle()
            args.putString(TAG, name)
            fragment.arguments = args

            return fragment
        }
    }

    @InjectPresenter
    lateinit var presenter: AboutUsPresenter

    @ProvidePresenter
    fun providePresenter() = AboutUsPresenter(
            (parentFragment as RouterProvider).getRouter()
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) = inflater.inflate(R.layout.fragment_about_us, container, false)!!

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onBackPressed(): Boolean {
        presenter.onBackPressed()
        return true
    }

}