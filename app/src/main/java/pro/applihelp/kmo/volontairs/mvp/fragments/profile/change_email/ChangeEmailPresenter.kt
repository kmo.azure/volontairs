package pro.applihelp.kmo.volontairs.mvp.fragments.profile.change_email

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import ru.terrakok.cicerone.Router

@InjectViewState
class ChangeEmailPresenter(private val router: Router): MvpPresenter<ChangeEmailView>() {

    fun onBackPressed() {
        router.exit()
    }

}