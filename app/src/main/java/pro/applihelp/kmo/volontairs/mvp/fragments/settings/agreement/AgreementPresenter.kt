package pro.applihelp.kmo.volontairs.mvp.fragments.settings.agreement

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import ru.terrakok.cicerone.Router

@InjectViewState
class AgreementPresenter(private val router: Router): MvpPresenter<AgreementView>() {

    fun onBackPressed() {
        router.exit()
    }

}