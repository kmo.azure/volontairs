package pro.applihelp.kmo.volontairs.domain.network.services

import io.reactivex.Observable
import pro.applihelp.kmo.volontairs.domain.network.models.EventExtendedModel
import pro.applihelp.kmo.volontairs.domain.network.models.EventsModel
import pro.applihelp.kmo.volontairs.domain.network.models.EventsShortModel
import pro.applihelp.kmo.volontairs.domain.network.APIs
import retrofit2.http.*

interface EventAPIs {

    @GET(APIs.EVENTS)
    fun eventsShortGet(
            @Query("page") page: String
    ): Observable<EventsShortModel>

    @GET(APIs.EVENTS)
    fun eventsGet(
            @Query("page") page: String
    ): Observable<EventsModel>

    @GET(APIs.EVENTS)
    fun eventExtendedGet(
            @Path("id", encoded = true) id: String
    ): Observable<EventExtendedModel>

    @POST(APIs.EVENTS)
    fun eventExtendedPost(
            @Body contacts: EventExtendedModel
    ): Observable<EventExtendedModel>

    @PUT(APIs.EVENTS)
    fun eventExtendedPut(
            @Body contacts: EventExtendedModel
    ): Observable<EventExtendedModel>

}