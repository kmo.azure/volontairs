package pro.applihelp.kmo.volontairs.mvp.fragments.events.event_datetime_contacts

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import ru.terrakok.cicerone.Router

@InjectViewState
class EventDateTimeContactsPresenter(private val router: Router): MvpPresenter<EventDateTimeContactsView>() {

    fun onBackPressed() {
        router.exit()
    }

}