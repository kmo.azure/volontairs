package pro.applihelp.kmo.volontairs.mvp.activities.loggedin

import android.content.Context
import android.os.Bundle
import android.support.v4.widget.DrawerLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import es.dmoral.toasty.Toasty
import pro.applihelp.kmo.volontairs.R
import pro.applihelp.kmo.volontairs.VolontairsApplication
import pro.applihelp.kmo.volontairs.mvp.adapters.drawer.DrawerAdapter
import pro.applihelp.kmo.volontairs.mvp.adapters.drawer.DrawerAdapter.ChoiceCallback
import pro.applihelp.kmo.volontairs.mvp.adapters.drawer.DrawerItemsModel
import pro.applihelp.kmo.volontairs.mvp.fragments.drawer_tabs.can_i_help.TabCanIHelpFragment
import pro.applihelp.kmo.volontairs.mvp.fragments.drawer_tabs.map.TabMapFragment
import pro.applihelp.kmo.volontairs.mvp.fragments.drawer_tabs.new_event.TabNewEventFragment
import pro.applihelp.kmo.volontairs.mvp.fragments.drawer_tabs.profile.TabProfileFragment
import pro.applihelp.kmo.volontairs.mvp.fragments.drawer_tabs.settings.TabSettingsFragment
import pro.applihelp.kmo.volontairs.navigation.BackButtonListener
import pro.applihelp.kmo.volontairs.navigation.Screens
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.commands.Back
import ru.terrakok.cicerone.commands.Command
import ru.terrakok.cicerone.commands.Replace
import ru.terrakok.cicerone.commands.SystemMessage
import javax.inject.Inject

class LoggedInActivity: MvpAppCompatActivity(), LoggedInView {

    @Inject
    lateinit var router: Router

    @Inject
    lateinit var navigatorHolder: NavigatorHolder

    @InjectPresenter
    lateinit var presenter: LoggedInPresenter

    @ProvidePresenter
    fun providePresenter() = LoggedInPresenter(router)

    private var mapTabFragment: TabMapFragment? = null
    private var canIHelpTabFragment: TabCanIHelpFragment? = null
    private var newEventTabFragment: TabNewEventFragment? = null
    private var profileTabFragment: TabProfileFragment? = null
    private var settingsTabFragment: TabSettingsFragment? = null

    private val context: Context = this
    private lateinit var drawer: DrawerLayout
    private lateinit var list: RecyclerView
    private val adapter = DrawerAdapter(arrayListOf(
            DrawerItemsModel(R.drawable.ic_launcher_background, "Map"),
            DrawerItemsModel(R.drawable.ic_launcher_background, "Can i help"),
            DrawerItemsModel(R.drawable.ic_launcher_background, "New event"),
            DrawerItemsModel(R.drawable.ic_launcher_background, "Profile"),
            DrawerItemsModel(R.drawable.ic_launcher_background, "Settings")
    ))

    override fun onCreate(savedInstanceState: Bundle?) {
        VolontairsApplication.INSTANCE.getAppComponent().inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_logged_in)

        initWindowBar()
        initToolBar()
        initViewComponents()
        initContainers()
        initDrawerLayout()

        if (savedInstanceState == null) {
            //bottomNavigationBar.selectTab(ANDROID_TAB_POSITION, true);
        }

        adapter.choiceCallback = object: ChoiceCallback {
            override fun getPosition(position: Int) {
                presenter.changeTab(position)
                presenter.closeDrawer()
            }
        }
    }

    private fun initToolBar() {

    }

    private fun initWindowBar() {

    }

    private fun initContainers() {
        val fm = supportFragmentManager

        mapTabFragment = fm.findFragmentByTag(resources.getString(R.string.tag_screen_tab_map)) as TabMapFragment?
        if (mapTabFragment == null) {
            mapTabFragment = TabMapFragment.getNewInstance(Screens.TAB_MAP_SCREEN)
            fm.beginTransaction()
                    .add(R.id.container_main, mapTabFragment, resources.getString(R.string.tag_screen_tab_map))
                    .detach(mapTabFragment)
                    .commitNow()
        }

        canIHelpTabFragment = fm.findFragmentByTag(resources.getString(R.string.tag_screen_tab_can_i_help)) as TabCanIHelpFragment?
        if (canIHelpTabFragment == null) {
            canIHelpTabFragment = TabCanIHelpFragment.getNewInstance(Screens.TAB_CAN_I_HELP_SCREEN)
            fm.beginTransaction()
                    .add(R.id.container_main, canIHelpTabFragment, resources.getString(R.string.tag_screen_tab_can_i_help))
                    .detach(canIHelpTabFragment)
                    .commitNow()
        }

        newEventTabFragment = fm.findFragmentByTag(resources.getString(R.string.tag_screen_tab_event_new)) as TabNewEventFragment?
        if (newEventTabFragment == null) {
            newEventTabFragment = TabNewEventFragment.getNewInstance(Screens.TAB_NEW_EVENT_SCREEN)
            fm.beginTransaction()
                    .add(R.id.container_main, newEventTabFragment, resources.getString(R.string.tag_screen_tab_event_new))
                    .detach(newEventTabFragment)
                    .commitNow()
        }

        profileTabFragment = fm.findFragmentByTag(resources.getString(R.string.tag_screen_tab_profile)) as TabProfileFragment?
        if (profileTabFragment == null) {
            profileTabFragment = TabProfileFragment.getNewInstance(Screens.TAB_PROFILE_SCREEN)
            fm.beginTransaction()
                    .add(R.id.container_main, profileTabFragment, resources.getString(R.string.tag_screen_tab_profile))
                    .detach(profileTabFragment)
                    .commitNow()
        }

        settingsTabFragment = fm.findFragmentByTag(resources.getString(R.string.tag_screen_tab_settings)) as TabSettingsFragment?
        if (settingsTabFragment == null) {
            settingsTabFragment = TabSettingsFragment.getNewInstance(Screens.TAB_SETTINGS_SCREEN)
            fm.beginTransaction()
                    .add(R.id.container_main, settingsTabFragment, resources.getString(R.string.tag_screen_tab_settings))
                    .detach(settingsTabFragment)
                    .commitNow()
        }
    }

    private fun initViewComponents() {
        drawer = findViewById(R.id.drawer_layout)
        list = findViewById(R.id.list)
    }

    private fun initDrawerLayout() {
        list.layoutManager = LinearLayoutManager(this)
        list.adapter = adapter
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()
        super.onPause()
    }

    override fun onBackPressed() {
        val fragment = supportFragmentManager.findFragmentById(R.id.container_main)
        if (fragment != null && fragment is BackButtonListener && (fragment as BackButtonListener).onBackPressed()) {
            return
        } else{
            super.onBackPressed()
        }
    }

    private var navigator = object: Navigator {

        override fun applyCommands(commands: Array<out Command>?) {
            commands?.forEach { applyCommand(it) }
        }

        private fun applyCommand(command: Command) {
            when (command::class.java) {
                Back::class.java -> backCommand()
                SystemMessage::class.java -> systemMessageCommand((command as SystemMessage).message)
                Replace::class.java -> replaceCommand(command)
            }
        }

        private fun backCommand() {
            finish()
        }

        private fun systemMessageCommand(message: String) {
            Toasty.info(context, message, Toast.LENGTH_SHORT, false).show()
        }

        private fun replaceCommand(command: Command) {
            val fm = supportFragmentManager
            when ((command as Replace).screenKey) {
                Screens.TAB_MAP_SCREEN ->
                    fm.beginTransaction()
                            .detach(newEventTabFragment)
                            .detach(profileTabFragment)
                            .detach(settingsTabFragment)
                            .detach(canIHelpTabFragment)
                            .attach(mapTabFragment)
                            .commitNow()

                Screens.TAB_CAN_I_HELP_SCREEN ->
                    fm.beginTransaction()
                            .detach(mapTabFragment)
                            .detach(newEventTabFragment)
                            .detach(profileTabFragment)
                            .detach(settingsTabFragment)
                            .attach(canIHelpTabFragment)
                            .commitNow()

                Screens.TAB_NEW_EVENT_SCREEN ->
                    fm.beginTransaction()
                            .detach(mapTabFragment)
                            .detach(canIHelpTabFragment)
                            .detach(profileTabFragment)
                            .detach(settingsTabFragment)
                            .attach(newEventTabFragment)
                            .commitNow()

                Screens.TAB_PROFILE_SCREEN ->
                    fm.beginTransaction()
                            .detach(mapTabFragment)
                            .detach(newEventTabFragment)
                            .detach(canIHelpTabFragment)
                            .detach(settingsTabFragment)
                            .attach(profileTabFragment)
                            .commitNow()

                Screens.TAB_SETTINGS_SCREEN ->
                    fm.beginTransaction()
                            .detach(mapTabFragment)
                            .detach(newEventTabFragment)
                            .detach(canIHelpTabFragment)
                            .detach(profileTabFragment)
                            .attach(settingsTabFragment)
                            .commitNow()
            }
        }
    }
    override fun closeDrawer() {
        drawer.closeDrawer(Gravity.END)
    }

    override fun openDrawer() {
        drawer.openDrawer(Gravity.END)
    }
}