package pro.applihelp.kmo.volontairs.mvp.fragments.profile.change_phone

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import ru.terrakok.cicerone.Router

@InjectViewState
class ChangePhonePresenter(private val router: Router): MvpPresenter<ChangePhoneView>() {

    fun onBackPressed() {
        router.exit()
    }

}