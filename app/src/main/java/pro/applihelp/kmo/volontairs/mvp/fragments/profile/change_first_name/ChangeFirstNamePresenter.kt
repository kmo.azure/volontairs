package pro.applihelp.kmo.volontairs.mvp.fragments.profile.change_first_name

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import ru.terrakok.cicerone.Router

@InjectViewState
class ChangeFirstNamePresenter(private val router: Router): MvpPresenter<ChangeFirstNameView>() {

    fun onBackPressed() {
        router.exit()
    }

}