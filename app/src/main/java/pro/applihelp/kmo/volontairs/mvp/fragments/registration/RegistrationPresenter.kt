package pro.applihelp.kmo.volontairs.mvp.fragments.registration

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter

@InjectViewState
class RegistrationPresenter: MvpPresenter<RegistrationView>() {
}