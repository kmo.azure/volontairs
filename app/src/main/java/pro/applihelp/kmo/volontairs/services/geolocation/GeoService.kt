package pro.applihelp.kmo.volontairs.services.geolocation

import android.Manifest.permission.ACCESS_COARSE_LOCATION
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.support.v4.app.ActivityCompat
import android.util.Log

class GeoService : Service() {

    companion object {
        private val TAG = GeoService::getResources.name
    }

    private var locationManager: LocationManager? = null
    private val locationListeners = arrayOf(LocationHelper(LocationManager.PASSIVE_PROVIDER))

    private val LOCATION_INTERVAL = 100L
    private val LOCATION_DISTANCE = 10F

    override fun onBind(intent: Intent) = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        return START_STICKY
    }

    override fun onCreate() {
        initLocationManager()

        try {
            locationManager?.requestLocationUpdates(
                    LocationManager.PASSIVE_PROVIDER,
                    LOCATION_INTERVAL,
                    LOCATION_DISTANCE,
                    locationListeners[0]
            )
        } catch (ex: SecurityException) {
            Log.i(TAG, "fail to request location update, ignore. ${ex.message}", ex)
        } catch (ex: IllegalArgumentException) {
            Log.d(TAG, "network provider does not exist, ${ex.message}", ex)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (locationManager != null) {
            locationListeners.forEach {
                try {
                    if (ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                            && ActivityCompat.checkSelfPermission(this, ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        locationManager!!.removeUpdates(it)
                    }
                } catch (ex: Exception) {
                    Log.i(TAG, "fail to remove location listener, ignore. ${ex.message}", ex)
                }
            }
        }
    }

    private fun initLocationManager() {
        if (locationManager == null) {
            locationManager = applicationContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        }
    }
}