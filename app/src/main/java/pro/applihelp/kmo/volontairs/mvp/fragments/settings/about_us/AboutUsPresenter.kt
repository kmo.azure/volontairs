package pro.applihelp.kmo.volontairs.mvp.fragments.settings.about_us

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import ru.terrakok.cicerone.Router

@InjectViewState
class AboutUsPresenter(private val router: Router): MvpPresenter<AboutUsView>() {

    fun onBackPressed() {
        router.exit()
    }

}