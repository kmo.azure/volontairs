package pro.applihelp.kmo.volontairs.domain.network.models

data class ContactModel(
        val first_name: String,
        val last_name: String,
        val phone: String,
        val email: String
)