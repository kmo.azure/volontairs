package pro.applihelp.kmo.volontairs.mvp.fragments.settings.settings_main

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import ru.terrakok.cicerone.Router

@InjectViewState
class SettingsMainPresenter(val router: Router): MvpPresenter<SettingsMainView>() {

    fun onBackPressed() {
        router.exit()
    }

}