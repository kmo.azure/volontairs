package pro.applihelp.kmo.volontairs.domain.network.models

open class EventModel(
        event_short: EventShortModel,
        val user: UserModel
): EventShortModel(eventShortModel = event_short) {

    constructor(event: EventModel): this(
            EventShortModel(
                    event.id,
                    event.type,
                    event.description,
                    event.is_my,
                    event.coordinates,
                    event.date_time
            ),
            event.user
    )

}