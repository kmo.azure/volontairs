package pro.applihelp.kmo.volontairs.mvp.fragments.profile.change_organization

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import ru.terrakok.cicerone.Router

@InjectViewState
class ChangeOrganizationPresenter(val router: Router): MvpPresenter<ChangeOrganizationView>() {

    fun onBackPressed() {
        router.exit()
    }

}