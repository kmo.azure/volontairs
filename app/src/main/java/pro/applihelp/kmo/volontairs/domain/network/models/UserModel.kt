package pro.applihelp.kmo.volontairs.domain.network.models

data class UserModel(
        val id: Long,
        val profile_photo: String,
        val organization: String,
        val contact: ContactModel
)