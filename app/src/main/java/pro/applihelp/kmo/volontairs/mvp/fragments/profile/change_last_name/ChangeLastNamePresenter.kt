package pro.applihelp.kmo.volontairs.mvp.fragments.profile.change_last_name

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import ru.terrakok.cicerone.Router

@InjectViewState
class ChangeLastNamePresenter(private val router: Router): MvpPresenter<ChangeLastNameView>() {

    fun onBackPressed() {
        router.exit()
    }

}