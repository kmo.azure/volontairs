package pro.applihelp.kmo.volontairs.data.mappers.login

import pro.applihelp.kmo.volontairs.domain.database.entities.LoginEntity
import pro.applihelp.kmo.volontairs.domain.network.models.ContactModel
import pro.applihelp.kmo.volontairs.domain.network.models.LoginModel
import pro.applihelp.kmo.volontairs.domain.network.models.NotificationModel
import pro.applihelp.kmo.volontairs.domain.network.models.UserModel

open class LoginMapper: ILoginMapper<LoginModel, LoginEntity> {
    override fun mapToEntity(type: LoginModel, is_login: Boolean) = LoginEntity(
            type.user.id,
            type.token,
            type.user.contact.email,
            type.user.contact.phone,
            type.user.profile_photo,
            type.user.contact.first_name,
            type.user.contact.last_name,
            type.user.organization,
            type.notif.notif_enable,
            type.notif.notif_start,
            type.notif.notif_end,
            is_login
    )

    override fun mapFromEntity(type: LoginEntity) = LoginModel(
            type.token,
            UserModel(
                    type.id,
                    type.profile_photo,
                    type.organization,
                    ContactModel(
                            type.first_name,
                            type.last_name,
                            type.phone,
                            type.email
                    )
            ),
            NotificationModel(
                    type.notif_enable,
                    type.notif_start,
                    type.notif_end
            )
    )
}