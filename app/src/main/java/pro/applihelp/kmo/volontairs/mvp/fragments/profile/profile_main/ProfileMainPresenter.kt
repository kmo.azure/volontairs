package pro.applihelp.kmo.volontairs.mvp.fragments.profile.profile_main

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import ru.terrakok.cicerone.Router

@InjectViewState
class ProfileMainPresenter(val router: Router): MvpPresenter<ProfileMainView>() {

    fun onBackPressed() {
        router.exit()
    }

}