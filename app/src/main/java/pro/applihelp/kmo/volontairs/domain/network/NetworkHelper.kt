package pro.applihelp.kmo.volontairs.domain.network

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class NetworkHelper {

    companion object {
        private var networkHelper: NetworkHelper? = null

        fun getInstance(): Retrofit? {
            var localNetworkHelper = networkHelper
            if (localNetworkHelper == null) {
                synchronized(NetworkHelper::class.java) {
                    localNetworkHelper = networkHelper
                    if (localNetworkHelper == null) {
                        networkHelper = NetworkHelper("")
                        localNetworkHelper = NetworkHelper("")
                    }
                }
            }

            return localNetworkHelper?.retrofit
        }

        fun setInstance(token: String) {
            networkHelper = NetworkHelper(token)
        }
    }

    private val retrofit: Retrofit
    private val test_url = "http://testurl.com/"
    private val release_url = "release url"

    private constructor(token: String) {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        retrofit = Retrofit.Builder()
                .baseUrl(test_url)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(initOkHttpClient(token, interceptor).build())
                .build()
    }

    private fun initOkHttpClient(token: String, interceptor: HttpLoggingInterceptor) =
        OkHttpClient.Builder()
                .readTimeout(15, TimeUnit.SECONDS)
                .connectTimeout(15, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)
                .addInterceptor(interceptor)
                .addInterceptor({
                    it.proceed(it.request()
                            .newBuilder()
                            .addHeader("Authorization", "JWT $token")
                            .removeHeader("User-Agent")
                            .removeHeader("Connection")
                            .method(it.request().method(), it.request().body())
                            .build()
                    )
                })
}